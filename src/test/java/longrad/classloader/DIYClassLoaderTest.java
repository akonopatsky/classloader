package longrad.classloader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;

import static org.junit.jupiter.api.Assertions.*;

class DIYClassLoaderTest {
    private DIYClassLoader diyClassLoader;

    @BeforeEach
    void setUp() {
        diyClassLoader = new DIYClassLoader();
    }

    @Test
    void defineClass() throws Exception {
        byte[] byteCode = diyClassLoader.readByteCodeFromFile("src/test/resources/TestClass.class");
        Class<?> clazz = diyClassLoader.defineClass("longrad.classloader.TestClass", byteCode);
        Constructor<?> constructor = clazz.getConstructor();
        Object object = constructor.newInstance();
        String resultString = (String) clazz.getMethod("action", String.class).invoke(object, "test1");
        assertEquals(resultString, "echo test1");
    }
}