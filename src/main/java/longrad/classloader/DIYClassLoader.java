package longrad.classloader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DIYClassLoader extends ClassLoader {

    public Class<?> defineClass(String className, byte[] bytecode) {
        return super.defineClass(className, bytecode, 0, bytecode.length)  ;
    }

    public byte[] readByteCodeFromFile(String fileName) throws IOException {
        File file = new File(fileName);
        return Files.readAllBytes(file.toPath());
    }

}
